import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PagesRoutes } from './core-routing.module';
import { MainLayoutComponent } from '../layouts/main-layout/main-layout.component';
import { SharedModule } from '../shared/shared.module';
import { ProgressBarModule } from 'primeng/progressbar';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';

@NgModule({
  declarations: [MainLayoutComponent],
  imports: [
    CommonModule,
    SharedModule,
    ProgressBarModule,
    ToastModule,
    RouterModule.forChild(PagesRoutes)
  ], providers: [MessageService]
})
export class CoreModule {}
