import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({ providedIn: 'root' })
export class SearchService {
  constructor(private apiService: ApiService) {}

  public search(route, searchedText: string) {
    if (route) {
      return this.apiService.request('get', `${route}?search=${searchedText}`);
    }
  }
}
