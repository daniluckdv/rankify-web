import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private http: HttpClient, private router: Router) {}

  public request(method: string, url: string, body?, useToken = true) {
    const options: any = { body };

    if (useToken) {
      options.headers = this.authHeaders();
    }

    return this.http.request(
      method.toUpperCase(),
      `${environment.apiUrl}/${url}`,
      options
    );
  }

  public logout() {
    localStorage.clear();
    this.router.navigate(['/sign-in']);
  }

  private authHeaders(): HttpHeaders {
    let headers = new HttpHeaders();
    headers = headers.append('Access-Control-Allow-Credentials', 'true');
    headers = headers.append('Authorization', this.token());
    return headers;
  }

  private token(): string {
    return `Bearer ${localStorage.getItem('token')}`;
  }
}
