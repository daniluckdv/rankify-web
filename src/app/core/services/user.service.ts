import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private apiService: ApiService) {}

  public getUserSettings(userId: string) {
    return this.apiService.request('GET', `users/${userId}`, {});
  }
  get user() {
    const user = JSON.parse(localStorage.getItem('user'));
    return user;
  }
}
