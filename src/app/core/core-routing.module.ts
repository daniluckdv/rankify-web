import { Routes } from '@angular/router';
import { MainLayoutComponent } from '../layouts/main-layout/main-layout.component';
import { IsLoggedGuard } from './guards/is-logged.guard';

export const PagesRoutes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    canActivate: [IsLoggedGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard'
      },
      {
        path: 'dashboard',
        loadChildren: '../pages/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'customers',
        loadChildren: '../pages/customers/customers.module#CustomersModule'
      },
      {
        path: 'campaigns',
        loadChildren: '../pages/campaigns/campaigns.module#CampaignsModule'
      },
      {
        path: 'reports',
        loadChildren: '../pages/reports/reports.module#ReportsModule'
      },
      {
        path: 'payments',
        loadChildren: '../pages/payments/payments.module#PaymentsModule'
      },
      {
        path: 'settings',
        loadChildren: '../pages/settings/settings.module#SettingsModule'
      },
      {
        path: 'support-tickets',
        loadChildren: '../pages/support/support.module#SupportModule'
      },
      {
        path: 'leads',
        loadChildren: '../pages/leads/leads.module#LeadsModule'
      },
      { path: 'stats', loadChildren: '../pages/stats/stats.module#StatsModule' }
    ]
  }
];
