import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class HeaderService {
  private headerData = new BehaviorSubject(null);
  private findedData = new BehaviorSubject(null);

  public currentHeaderData = this.headerData.asObservable();
  public getFindedData = this.findedData.asObservable();
  
  public changeHeaderData(router: string) {
    this.headerData.next(router);
  }
  
  public setFindedData(data: any) {
    this.findedData.next(data);
  }
}
