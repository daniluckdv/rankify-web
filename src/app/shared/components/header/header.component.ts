import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../../core/services/user.service';
import { SearchService } from 'src/app/core/services/search.service';
import { HeaderService } from './header.service';
import { fromEvent } from 'rxjs';
import { map, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @ViewChild('searchInput') searchInput: ElementRef;

  private activeRoute: string;
  public user;

  constructor(
    private readonly router: Router,
    private readonly userService: UserService,
    private readonly searchService: SearchService,
    private readonly headerService: HeaderService
  ) {
    this.user = this.userService.user;
  }

  ngOnInit() {
    this.headerService.currentHeaderData.subscribe(
      route => (this.activeRoute = route)
    );

    fromEvent(this.searchInput.nativeElement, 'keyup')
      .pipe(
        debounceTime(1000),
        map((event: any) => event.target.value)
      )
      .subscribe(text => this.find(text));
  }

  public find(text) {
    if (this.activeRoute) {
      this.searchService
        .search(this.activeRoute, text)
        .subscribe((res: any) => this.headerService.setFindedData(res));
    }
  }

  public logout() {
    localStorage.clear();
    this.router.navigate(['/sign-in']);
  }
}
