import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.scss']
})
export class DatatableComponent implements OnInit {
  @Output() delete = new EventEmitter<string>();
  @Input() cols = [];
  @Input() data = [];
  @Input() editable: boolean;
  @Input() deletable: boolean;
  @Input() loading: boolean;
  @Input() total: any;

  constructor(private confirmationService: ConfirmationService) {}

  ngOnInit() {}

  public setClasses(item: any, field: string) {
    return {
      active: field === 'Active' || field === 'Paid' || field === 'Resolved' ,
      disabled:
        field === 'Disabled' ||
        field === 'Unpaid' ||
        field === 'Cancelled' ||
        field === 'Pause',
      resolved:
        field === 'Open' || field === 'Expired' || field === 'Suspended',
      error: field === 'Overdue',
      new: field === 'New',
      boldFont: item.clientName === field || item.service === field,
      'green-color': item.amount === field,
      'red-color': item.invoice_status === 'Overdue' && item.amount === field
    };
  }

  public remove(id: string) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      accept: () => {
        this.delete.emit(id);
      }
    });
  }
}
