import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../../../core/services/user.service';
import { SideBarService } from './side-bar.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  public user: any;
  public menuItems: any[];

  constructor(
    private userService: UserService,
    private sideBarService: SideBarService
  ) {
    this.user = this.userService.user;
    this.menuItems = [
      {
        name: 'Dashboard',
        icon: './assets/images/icons/dashboard.png',
        routerLink: '/dashboard'
      },
      {
        name: 'Campaigns',
        icon: './assets/images/icons/campaigns.png',
        routerLink: '/campaigns'
      }
    ];

    if (this.user.role === 'admin' || this.user.role === 'manager') {
      this.menuItems.push(
        {
          name: 'Customers',
          icon: './assets/images/icons/customers.png',
          routerLink: '/customers'
        },
        {
          name: 'Reports',
          icon: './assets/images/icons/reports.png',
          routerLink: '/reports'
        },
        {
          name: 'Payments',
          icon: './assets/images/icons/payments.png',
          routerLink: '/payments'
        }
      );
    } else {
      this.menuItems.push(
        {
          name: 'Payments',
          icon: './assets/images/icons/payments.png',
          routerLink: '/payments'
        },
        {
          name: 'Support',
          icon: './assets/images/icons/client-support.png',
          routerLink: '/support-tickets'
        },
        {
          name: 'Leads',
          icon: './assets/images/icons/phone.png',
          routerLink: '/leads'
        }
      );
    }

    this.menuItems.push({
      name: 'Settings',
      icon: './assets/images/icons/client-settings.png',
      routerLink: '/settings'
    });
  }

  ngOnInit() {
    this.getUserData();
    this.sideBarService.changeSideBarData(this.userService.user);
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }

    this.sideBarService.changeSideBarData({});
  }

  private getUserData() {
    const subscription = this.sideBarService.currentSideBarData.subscribe(
      res => {
        if (res.image) {
          this.user.photo_path = res.image;
        } else {
          this.user.photo_path = this.userService.user.photo_path
            ? this.userService.user.photo_path
            : 'assets/images/icons/user-without-image.png';
        }

        if (res.plan) {
          this.user.plan = res.plan;
        }

        if (res.firstName) {
          this.user.firstName = res.firstName;
        } else {
          this.user.firstName = this.userService.user.first_name;
        }

        if (res.lastName) {
          this.user.lastName = res.lastName;
        } else {
          this.user.lastName = this.userService.user.last_name;
        }
      }
    );

    this.subscriptions.push(subscription);
  }
}
