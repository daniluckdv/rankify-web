import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UserService } from '../../../core/services/user.service';

@Injectable({
  providedIn: 'root'
})
export class SideBarService {
  constructor(private readonly userService: UserService) {}
  
  private sideBarData = new BehaviorSubject({
    firstName: this.userService.user.first_name,
    lastName: this.userService.user.last_name,
    plan: this.userService.user.plan,
    image: this.userService.user.photo_path
      ? this.userService.user.photo_path
      : 'assets/images/icons/user-without-image.png'
  });
  public currentSideBarData = this.sideBarData.asObservable();

  public changeSideBarData(data: any) {
    this.sideBarData.next(data);
  }
}
