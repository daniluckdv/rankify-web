import { Injectable } from '@angular/core';
import { ApiService } from '../../../core/services/api.service';

@Injectable()
export class ReportsService {
  constructor(private apiService: ApiService) {}

  public getReports() {
    return this.apiService.request('GET', 'report?year=2019&month=5', {});
  }
}
