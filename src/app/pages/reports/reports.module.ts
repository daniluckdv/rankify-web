import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsComponent } from './pages/reports.component';
import { ReportsRoutingModule } from './reports-routing.module';
import { ReportsService } from './services/reports.service';
import { ProgressSpinnerModule } from 'primeng/progressspinner';

@NgModule({
  declarations: [ReportsComponent],
  imports: [CommonModule, ProgressSpinnerModule, ReportsRoutingModule],
  providers: [ReportsService]
})
export class ReportsModule {}
