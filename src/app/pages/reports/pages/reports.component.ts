import { Component, OnInit } from '@angular/core';
import { ReportsService } from '../services/reports.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
  public plansList: any[];
  public clientCardsList: any[];
  public loading = true;

  constructor(private reportsService: ReportsService) {
    this.plansList = [
      {
        title: 'Standard plan',
        key: 'standard'
      },
      {
        title: 'Business plan',
        key: 'business'
      },
      {
        title: 'Premium plan',
        key: 'premium'
      }
    ];

    this.clientCardsList = [
      {
        title: 'New customers',
        key: 'new_customers',
        icon: 'assets/images/icons/user.png',
        class: 'new'
      },
      {
        title: 'Cancelled users',
        key: 'canceled_customers',
        icon: 'assets/images/icons/close.png',
        class: 'cancelled'
      },
      {
        title: 'Paused clients',
        key: 'paused_customers',
        icon: 'assets/images/icons/pause.png',
        class: 'paused'
      }
    ];
  }

  ngOnInit() {
    this.getReports();
  }

  private getReports() {
    this.reportsService.getReports().subscribe((res: any) => {
      for (let clientCard of this.clientCardsList) {
        clientCard.counter = res.data[clientCard.key];
      }

      for (let plan of this.plansList) {
        plan.counter = res.data.customers_total[plan.key];
        plan.monthlyRevenue = res.data.customers_amount[plan.key];
      }

      this.loading = false;
    });
  }
}
