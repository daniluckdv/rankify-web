import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-settings',
  template:
    '<router-outlet></router-outlet><p-toast position="top-right"></p-toast>'
})
export class SettingsComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
