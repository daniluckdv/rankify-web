import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SettingsService } from '../../../services/settings.service';
import { MessageService, ConfirmationService } from 'primeng/api';
import { UserService } from '../../../../../core/services/user.service';
import { SideBarService } from '../../../../../shared/components/side-bar/side-bar.service';

@Component({
  selector: 'app-profile-settings',
  templateUrl: './profile-settings.component.html',
  styleUrls: ['./profile-settings.component.scss']
})
export class ProfileSettingsComponent implements OnInit {
  public user: any;
  public files: any[];
  public plansList: any[];
  public settingsForm: FormGroup;
  public loading = true;

  constructor(
    private formBuilder: FormBuilder,
    private settingsService: SettingsService,
    private userService: UserService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private sideBarService: SideBarService
  ) {
    this.user = this.userService.user;
    this.settingsForm = this.formBuilder.group({
      email: [null, Validators.compose([Validators.required])],
      phone: [null, Validators.compose([Validators.required])],
      first_name: [null, Validators.compose([Validators.required])],
      last_name: [null, Validators.compose([Validators.required])],
      username: [null, Validators.compose([Validators.required])],
      business_name: [null, Validators.compose([Validators.required])],
      status: [null, Validators.compose([Validators.required])],
      plan: [null, Validators.compose([Validators.required])],
      password: [null]
    });

    this.settingsForm.get('email').disable();
    this.settingsForm.get('phone').disable();
  }

  ngOnInit() {
    this.user.status = this.user.status.toString();
    this.settingsForm.patchValue(this.user);
    this.getPlans();
  }

  public updateUser(showNotification = true) {
    this.settingsForm.get('email').enable();
    this.settingsForm.get('phone').enable();

    if (!this.settingsForm.value.password) {
      this.settingsForm.removeControl('password');
    }

    this.settingsService
      .updateUser(this.settingsForm.value, this.user.id)
      .subscribe((res: any) => {
        const firstName = this.settingsForm.value.first_name;
        const lastName = this.settingsForm.value.last_name;
        this.settingsForm.get('email').disable();
        this.settingsForm.get('phone').disable();
        this.sideBarService.changeSideBarData({ firstName, lastName });
        if (showNotification) {
          this.messageService.add({
            severity: 'success',
            summary: res.message,
            closable: true
          });
        }
      });
  }

  public selectAvatar(files) {
    let image: string;
    const formData = new FormData();
    formData.append('photo', files[0]);
    this.files = null;
    this.user.photo_path = null;

    this.settingsService
      .addUserPhoto(formData, this.user.id)
      .subscribe((res: any) => {
        const user = this.userService.user;
        this.settingsService
          .getUserSettings(this.user.id)
          .subscribe((res: any) => {
            image = res.data.photo_path;
            user.photo_path = image;
            this.sideBarService.changeSideBarData({ image });
          });

        this.messageService.add({
          severity: 'success',
          summary: res.message,
          closable: true
        });

        localStorage.setItem('user', JSON.stringify(user));
      });

    return (this.files = files);
  }

  public deletePhoto() {
    if (this.files || this.user.photo_path) {
      this.settingsService
        .deleteUserPhoto(this.user.id)
        .subscribe((res: any) => {
          const user = this.userService.user;
          this.files = null;
          this.user.photo_path = null;
          user.photo_path = null;
          localStorage.setItem('user', JSON.stringify(user));

          this.messageService.add({
            severity: 'success',
            summary: res.message,
            closable: true
          });

          this.sideBarService.changeSideBarData({
            image: 'assets/images/icons/user-without-image.png'
          });
        });
    }
  }

  public updateCreditCard() {
    this.messageService.add({
      severity: 'info',
      summary: 'Feature in development',
      closable: true
    });
  }

  public changePlan(plan) {
    this.confirmationService.confirm({
      key: 'plan',
      message: 'Please confirm your Rankify plan change?',
      accept: () => {
        this.settingsForm.get('plan').patchValue(plan);
        this.updateUser(false);

        this.messageService.add({
          severity: 'success',
          summary: 'Thank you, your plan has been changed',
          closable: true
        });

        const user = this.userService.user;
        user.plan = plan;
        localStorage.setItem('user', JSON.stringify(user));
        this.sideBarService.changeSideBarData({ plan });
      }
    });
  }

  private getPlans() {
    this.settingsService.getPlans().subscribe((res: any) => {
      this.plansList = res.data;
      this.loading = false;
    });
  }
}
