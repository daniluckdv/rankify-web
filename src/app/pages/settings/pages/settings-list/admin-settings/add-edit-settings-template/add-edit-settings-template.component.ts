import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SettingsService } from '../../../../services/settings.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-add-edit-settings-template',
  templateUrl: './add-edit-settings-template.component.html',
  styleUrls: ['./add-edit-settings-template.component.scss']
})
export class AddEditSettingsTemplateComponent implements OnInit {
  private templateId: string;

  public editTemplateForm: FormGroup;
  public loading = true;
  
  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private settingsService: SettingsService,
    private messageService: MessageService,
    private router: Router
  ) {
    this.editTemplateForm = this.formBuilder.group({
      title: [null, Validators.compose([Validators.required])],
      subject: [null, Validators.compose([Validators.required])],
      body: [null, Validators.compose([Validators.required])]
    });

    this.activatedRoute.paramMap.subscribe((res: any) => {
      this.templateId = res.params.id;
    });
  }

  ngOnInit() {
    this.getTemplate();
  }

  public updateTemplate() {
    const body = this.editTemplateForm.value;

    this.settingsService
      .updateCreateTemplate(body, this.templateId)
      .subscribe((res: any) => {
        this.messageService.add({
          severity: 'success',
          summary: res.message,
          closable: true
        });
      });

      this.router.navigate(['/settings']);
  }

  private getTemplate() {
    if (this.templateId) {
      this.settingsService.getTemplate(this.templateId).subscribe((res: any) => {
        this.editTemplateForm.patchValue(res.data);
        this.loading = false;
      });
    } else {
      this.loading = false;
    }
  }
}
