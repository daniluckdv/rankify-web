import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../services/settings.service';
import { MessageService, ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-admin-settings',
  templateUrl: './admin-settings.component.html',
  styleUrls: ['./admin-settings.component.scss']
})
export class AdminSettingsComponent implements OnInit {
  public templateList: any[];
  public plansList: any[];
  public loading = true;

  constructor(
    private settingsService: SettingsService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService
  ) {}

  ngOnInit() {
    this.getTemplates();
    this.getPlans();
  }

  public removeTemplate(id: string) {
    this.confirmationService.confirm({
      key: 'template',
      message: 'Are you sure that you want to perform this action?',
      accept: () => {
        this.settingsService.deleteTemplate(id).subscribe((res: any) => {
          this.templateList = this.templateList.filter(
            template => template.id !== id
          );
          this.messageService.add({
            severity: 'success',
            summary: res.message,
            closable: true
          });
        });
      }
    });
  }

  private getTemplates() {
    this.settingsService.getTemplates().subscribe((res: any) => {
      this.templateList = res.data;
      this.loading = false;
    });
  }

  private getPlans() {
    this.settingsService.getPlans().subscribe((res: any) => {
      this.plansList = res.data;
    });
  }
}
