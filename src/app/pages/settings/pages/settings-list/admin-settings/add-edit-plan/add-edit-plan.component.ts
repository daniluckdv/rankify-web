import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SettingsService } from 'src/app/pages/settings/services/settings.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-add-edit-plan',
  templateUrl: './add-edit-plan.component.html',
  styleUrls: ['./add-edit-plan.component.scss']
})
export class AddEditPlanComponent implements OnInit {
  public plan: string
  public editPlanForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private settingsService: SettingsService,
    private messageService: MessageService
  ) {
    this.activatedRoute.paramMap.subscribe(res => this.plan = res.get('plan'));
    
    this.editPlanForm = this.formBuilder.group({
      title: [null, Validators.compose([Validators.required])],
      price: [null, Validators.compose([Validators.required])],
    });
  }

  ngOnInit() {
    this.getPlan();
  }

  public editPlan() {
    this.settingsService.editPlan(this.plan, this.editPlanForm.value).subscribe((res: any) => {
      this.router.navigate(['/settings']);
      this.messageService.add({
        severity: 'success',
        summary: res.message,
        closable: true
      });
    });
  }

  private getPlan() {
    this.settingsService.getPlan(this.plan).subscribe((res: any) => {
      this.editPlanForm.patchValue(res.data);
    }); 
  }
}
