import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../core/services/user.service';

@Component({
  selector: 'app-settings-list',
  templateUrl: './settings-list.component.html'
})
export class SettingsListComponent implements OnInit {
  public user: any;

  constructor(private userService: UserService) {
    this.user = this.userService.user;
  }

  ngOnInit() {}
}
