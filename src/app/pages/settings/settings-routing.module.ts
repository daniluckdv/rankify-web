import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingsComponent } from './pages/settings.component';
import { AddEditSettingsTemplateComponent } from './pages/settings-list/admin-settings/add-edit-settings-template/add-edit-settings-template.component';
import { SettingsListComponent } from './pages/settings-list/settings-list.component';
import { ProfileSettingsComponent } from './pages/settings-list/profile-settings/profile-settings.component';
import { AddEditPlanComponent } from './pages/settings-list/admin-settings/add-edit-plan/add-edit-plan.component';

const routes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    children: [
      { path: '', component: SettingsListComponent },
      { path: 'create', component: AddEditSettingsTemplateComponent },
      { path: 'edit-template/:id', component: AddEditSettingsTemplateComponent },
      { path: 'edit-profile', component: ProfileSettingsComponent },
      { path: 'edit-plan/:plan', component: AddEditPlanComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule {}
