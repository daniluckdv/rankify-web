import { Injectable } from '@angular/core';
import { ApiService } from '../../../core/services/api.service';

@Injectable()
export class SettingsService {
  constructor(private apiService: ApiService) {}

  public getTemplates() {
    return this.apiService.request('GET', 'templates?offset=0&limit=1000', {});
  }

  public getTemplate(id: string) {
    return this.apiService.request('GET', `templates/${id}`, {});
  }

  public addUserPhoto(files, id?: string) {
    if (id) {
      return this.apiService.request('POST', `users/${id}/photo`, files);
    } else {
      return this.apiService.request('POST', 'users/photo', files);
    }
  }

  public deleteUserPhoto(id: string) {
    return this.apiService.request('DELETE', `users/${id}/photo`, {});
  }

  public deleteTemplate(id: string) {
    return this.apiService.request('DELETE', `templates/${id}`, {});
  }

  public updateCreateTemplate(body, id?: string) {
    if (id) {
      return this.apiService.request('POST', `templates/${id}`, body);
    } else {
      return this.apiService.request('POST', 'templates', body);
    }
  }

  public getUserSettings(userId: string) {
    return this.apiService.request('GET', `users/${userId}`, {});
  }

  public updateUser(body, userId: string) {
    body._method = 'patch';
    return this.apiService.request('POST', `users/${userId}`, body);
  }

  public getPlans() {
    return this.apiService.request('GET', 'plans');
  }

  public getPlan(planName: string) {
    return this.apiService.request('GET', `plans/${planName}`);
  }

  public editPlan(planName: string, body) {
    body._method = 'patch';
    return this.apiService.request('POST', `plans/${planName}`, body);
  }
}
