import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './pages/settings.component';
import { AddEditSettingsTemplateComponent } from './pages/settings-list/admin-settings/add-edit-settings-template/add-edit-settings-template.component';
import { ButtonModule } from 'primeng/button';
import { EditorModule } from 'primeng/editor';
import { ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { AdminSettingsComponent } from './pages/settings-list/admin-settings/admin-settings.component';
import { SharedModule } from 'primeng/components/common/shared';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { DropdownModule } from 'primeng/dropdown';
import { FileUploadModule } from 'primeng/fileupload';
import { SettingsService } from './services/settings.service';
import { SettingsListComponent } from './pages/settings-list/settings-list.component';
import { MessageService, ConfirmationService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ProfileSettingsComponent } from './pages/settings-list/profile-settings/profile-settings.component';
import { AddEditPlanComponent } from './pages/settings-list/admin-settings/add-edit-plan/add-edit-plan.component';

@NgModule({
  declarations: [
    SettingsComponent,
    AddEditSettingsTemplateComponent,
    AdminSettingsComponent,
    ProfileSettingsComponent,
    SettingsListComponent,
    AddEditPlanComponent
  ],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    ReactiveFormsModule,
    EditorModule,
    InputTextModule,
    ButtonModule,
    SharedModule,
    InputTextareaModule,
    DropdownModule,
    FileUploadModule,
    ToastModule,
    ProgressSpinnerModule,
    ConfirmDialogModule
  ],
  providers: [SettingsService, MessageService, ConfirmationService]
})
export class SettingsModule {}
