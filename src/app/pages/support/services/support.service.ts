import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/core/services/api.service';

interface ITicket {
  campaign_id: string;
  subject: string;
  description: string;
  ticket_status: string;
  _method?: string;
}

@Injectable()
export class SupportService {
  constructor(private apiService: ApiService) {}

  public getCampaigns(customerId) {
    return this.apiService.request('GET', `campaigns?offset=0&limit=100&${customerId}`, {});
  }

  public getTickets(customerId: string) {
    return this.apiService.request('GET', `tickets?customer_id=${customerId}`, {});
  }
  
  public getTicket(ticketId: string) {
    return this.apiService.request('GET', `tickets/${ticketId}`, {});
  }
  
  public getComments(ticketId: string) {
    return this.apiService.request('GET', `tickets/${ticketId}/comments`, {});
  }
  
  public sentMessage(ticketId: string, body) {
    return this.apiService.request('POST', `tickets/${ticketId}/comments`, body);
  }
  
  public createUpdateTicket(body: ITicket, ticketId?: string) {
    if (ticketId) {
      body._method = 'patch';
      return this.apiService.request('POST', `tickets/${ticketId}`, body);
    } else {
      return this.apiService.request('POST', `tickets`, body);
    }
  }
}
