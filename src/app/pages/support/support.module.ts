import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TicketsComponent } from './pages/tickets/tickets.component';

import { TicketComponent } from './pages/ticket/ticket.component';
import { SupportRoutingModule } from './support-routing';
import { SupportComponent } from './support.component';
import { ButtonModule } from 'primeng/button';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ReactiveFormsModule } from '@angular/forms';
import { SupportService } from './services/support.service';
import { DropdownModule } from 'primeng/dropdown';
import { SharedModule } from '../../shared/shared.module';
import { InputTextModule } from 'primeng/inputtext';
import { ProgressSpinnerModule } from 'primeng/progressspinner';

@NgModule({
  declarations: [SupportComponent, TicketsComponent, TicketComponent],
  imports: [
    CommonModule,
    ButtonModule,
    ReactiveFormsModule,
    SupportRoutingModule,
    DropdownModule,
    InputTextareaModule,
    SharedModule,
    InputTextModule,
    ProgressSpinnerModule
  ],
  providers: [SupportService]
})
export class SupportModule {}
