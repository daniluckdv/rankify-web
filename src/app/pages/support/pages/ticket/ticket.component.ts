import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SupportService } from '../../services/support.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { UserService } from '../../../../core/services/user.service';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss']
})
export class TicketComponent implements OnInit {
  public ticketId: string;
  public message: string;
  public editTicketForm: FormGroup;
  public statusList = [];
  public campaignsList = [];
  public messagesList = [];
  public loading = true;

  constructor(
    private activatedRoute: ActivatedRoute,
    private supportService: SupportService,
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private userService: UserService
  ) {
    this.editTicketForm = this.formBuilder.group({
      subject: [null, Validators.compose([Validators.required])],
      ticket_status: [null, Validators.compose([Validators.required])],
      campaign_id: [null, Validators.compose([Validators.required])],
      description: [null, Validators.compose([Validators.required])],
      message: [null]
    });
    this.activatedRoute.paramMap.subscribe((res: any) => {
      this.ticketId = res.get('ticketId');
    });

    this.statusList = [
      { label: 'Active', value: '1' },
      { label: 'Resolved', value: '2' }
    ];
  }

  ngOnInit() {
    this.getTicket();
    this.getCampaigns();
  }

  public createUpdateTicket() {
    this.supportService
      .createUpdateTicket(this.editTicketForm.value, this.ticketId)
      .subscribe((res: any) => {
        history.back();
        this.messageService.add({
          severity: 'success',
          summary: res.message,
          closable: true
        });
      });
  }

  public sentMessage() {
    this.supportService
      .sentMessage(this.ticketId, {
        message: this.editTicketForm.value.message
      })
      .subscribe(() => this.getComments());
  }

  private getTicket() {
    if (this.ticketId) {
      this.getComments();

      this.supportService.getTicket(this.ticketId).subscribe((res: any) => {
        this.campaignsList.push({
          label: `${res.data.customer_name} (${res.data.campaign_type})`,
          value: res.data.campaign_id.toString()
        });
        res.data.ticket_status = res.data.ticket_status.toString();
        this.editTicketForm.patchValue(res.data);
        this.loading = false;
      });
    } else {
      this.editTicketForm
        .get('ticket_status')
        .patchValue(this.statusList[0].value);
      this.loading = false;
    }
  }

  private getCampaigns() {
    this.supportService
      .getCampaigns(this.userService.user.id)
      .subscribe((res: any) => {
        
        for (const campaign of res.data) {
          if (
            campaign.campaign_type === 'standard' ||
            campaign.campaign_type === 'google'
          ) {
            campaign.campaign_type = 'Google';
          } else if (campaign.campaign_type === 'facebook') {
            campaign.campaign_type = 'Facebook';
          } else if (campaign.campaign_type === 'call_rails') {
            campaign.campaign_type = 'Call rails';
          }

          this.campaignsList.push({
            label: `${campaign.customer_name} (${campaign.campaign_type}) ID: ${
              campaign.id
            }`,
            value: campaign.id.toString()
          });
        }

        this.loading = false;
      });
  }

  private getComments() {
    this.supportService.getComments(this.ticketId).subscribe((res: any) => {
      if (this.userService.user.role === 'customer') {
        this.editTicketForm.get('subject').disable();
        this.editTicketForm.get('description').disable();
      }
      this.messagesList = res.data.reverse();
    });
  }
}
