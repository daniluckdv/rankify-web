import { Component, OnInit, OnDestroy } from '@angular/core';
import { SupportService } from '../../services/support.service';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../../../core/services/user.service';
import { HeaderService } from 'src/app/shared/components/header/header.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.scss']
})
export class TicketsComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  private customerId: string;

  public loading = true;
  public tableHeaders: any[];
  public supportTicketsList = [];

  constructor(
    public userService: UserService,
    private supportService: SupportService,
    private activatedRoute: ActivatedRoute,
    private headerService: HeaderService
  ) {
    this.activatedRoute.paramMap.subscribe(
      res => (this.customerId = res.get('id'))
    );

    this.tableHeaders = [
      { header: 'Campaign', field: 'campaign_type' },
      { header: 'Subject', field: 'subject' },
      { header: 'Created', field: 'created_at' },
      { header: 'Status', field: 'ticket_status' }
    ];
  }

  ngOnInit() {
    this.headerService.changeHeaderData('tickets');
    this.getDataFromSearch();
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }

    this.headerService.setFindedData(null);
    this.headerService.changeHeaderData(null);
  }

  private parseTickets(data) {
    for (const ticket of data) {
      if (ticket.ticket_status === 1) {
        ticket.ticket_status = 'Open';
      } else {
        ticket.ticket_status = 'Resolved';
      }

      if (ticket.campaign_type === 'standard') {
        ticket.campaign_type = `Google. ID: ${ticket.campaign_id}`;
      } else {
        ticket.campaign_type = `${ticket.campaign_type}. ID: ${
          ticket.campaign_id
        }`;
      }
    }

    this.supportTicketsList = data;
    this.loading = false;
  }

  private getTicketsFromAPI() {
    const id = this.customerId ? this.customerId : this.userService.user.id;
    const getTicketsSubscribe = this.supportService
      .getTickets(id)
      .subscribe((res: any) => {
        this.parseTickets(res.data);
      });

    this.subscriptions.push(getTicketsSubscribe);
  }

  private getDataFromSearch() {
    const getSearchedData = this.headerService.getFindedData.subscribe(res => {
      if (res) {
        this.supportTicketsList = [];
        this.parseTickets(res);
      } else {
        this.getTicketsFromAPI();
      }
    });

    this.subscriptions.push(getSearchedData);
  }
}
