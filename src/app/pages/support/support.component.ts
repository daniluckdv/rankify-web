import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-support',
  template: '<router-outlet></router-outlet>'
})
export class SupportComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
