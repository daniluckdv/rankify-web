import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../core/services/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {
  public user: any;

  constructor(private userService: UserService) {
    this.user = this.userService.user;
  }

  ngOnInit() {}
}
