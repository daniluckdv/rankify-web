import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../services/dashboard.service';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'client-dashboard',
  templateUrl: './dashboard-client.component.html',
  styleUrls: ['./dashboard-client.component.scss']
})
export class DashboardClientComponent implements OnInit {
  public cardList: any[];

  public campaignsCols: any[];
  public campaignsList: any[];

  public lastPaymentsCols: any[];
  public paymentsList: any[];
  public loading = true;

  constructor(
    private dashboardService: DashboardService,
    private userService: UserService
  ) {
    this.cardList = [
      {
        title: 'Google Ads campaigns',
        counter: '01',
        key: 'google_campaigns_total',
        icon: 'assets/images/icons/dashboard-active-customers.png'
      },
      {
        title: 'Facebook campaigns',
        key: 'facebook_campaigns_total',
        counter: '02'
      },
      {
        title: 'Call Rail campaigns',
        key: 'call_campaigns_total',
        counter: '01'
      }
    ];

    this.campaignsCols = [
      { header: 'Campaign Title', field: 'campaign_title' },
      { header: 'Created', field: 'created_at' },
      { header: 'Type', field: 'campaign_type' },
      { header: 'Status', field: 'campaign_status' }
    ];

    this.lastPaymentsCols = [
      { header: 'Service', field: 'campaign_type' },
      { header: 'Date of payment', field: 'created_at' },
      { header: 'Payment method', field: 'payment_type' },
      { header: 'Amount', field: 'amount' },
      { header: 'Status', field: 'invoice_status' }
    ];
  }

  ngOnInit() {
    this.getCampaigns();
    this.getInvoices();
    this.getUserCampaigns();
  }

  private getCampaigns() {
    this.dashboardService.getCampaigns().subscribe((res: any) => {
      for (let card of this.cardList) {
        card.counter = res.data[card.key];
        this.loading = false;
      }
    });
  }

  private getInvoices() {
    this.dashboardService
      .getUserInvoices(this.userService.user.id)
      .subscribe((res: any) => {
        res.data.forEach(payment => {
          payment.amount = '$ ' + payment.amount.toFixed(2).toString();
          payment.created_at = payment.payment_date;
          payment.campaign_type = payment.campaign_type + '. ID: ' + payment.campaign_id;

          if (payment.campaign_type === 'standard') {
            payment.campaign_type = 'Google';
          }
          
          if (payment.payment_type === 'standard') {
            payment.payment_type = 'Invoice';
          }
  
          if (payment.invoice_status === 1) {
            payment.invoice_status = 'Active';
          } else {
            payment.invoice_status = 'Overdue';
          }
        });

        this.paymentsList = res.data.filter(invoice => {
          return invoice.invoice_status === 2 || invoice.invoice_status === 0 || invoice.invoice_status === 'Overdue';
        });
        this.loading = false;
      });
  }

  private getUserCampaigns() {
    this.dashboardService
      .getUserCampaigns(this.userService.user.id)
      .subscribe((res: any) => {
        for (const campaign of res.data) {
          if (campaign.campaign_status === 1) {
            campaign.campaign_status = 'Active';
          } else if (campaign.campaign_status === 3) {
            campaign.campaign_status = 'Disabled';
          } else if (campaign.campaign_status === 2) {
            campaign.campaign_status = 'Suspended';
          }

          if (
            campaign.campaign_type === 'standard' ||
            campaign.campaign_type === 'google'
          ) {
            campaign.campaign_type = 'Google Ads';
          }

          if (campaign.campaign_type === 'facebook') {
            campaign.campaign_type = 'Facebook Ads';
          }

          if (campaign.campaign_type === 'call_rails') {
            campaign.campaign_type = 'Call Rail Ads';
          }
        }
        this.campaignsList = res.data;
        this.loading = false;
      });
  }
}
