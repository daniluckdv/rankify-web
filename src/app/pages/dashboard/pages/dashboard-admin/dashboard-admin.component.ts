import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../services/dashboard.service';
import { strictEqual } from 'assert';

@Component({
  selector: 'app-dashboard-admin',
  templateUrl: './dashboard-admin.component.html',
  styleUrls: ['./dashboard-admin.component.scss']
})
export class DashboardAdminComponent implements OnInit {
  public cardList: any[];
  public activityList = [];
  public chart: any;
  public loading = true;

  constructor(private dashboardService: DashboardService) {
    this.cardList = [
      {
        title: 'Active Campaigns',
        key: 'active_campaigns',
        icon: 'assets/images/icons/dashboard-active-customers.png',
        campaignType: 'active'
      },
      {
        title: 'Suspended Campaigns',
        key: 'suspended_campaigns',
        icon: 'assets/images/icons/dashboard-suspended-customers.png',
        campaignType: 'suspended'
      },
      {
        title: 'Cancelled Campaigns',
        key: 'canceled_campaigns',
        icon: 'assets/images/icons/dashboard-cancelled-customers.png',
        campaignType: 'cancelled'
      }
    ];
  }

  ngOnInit() {
    this.getCustomersChart();
    this.getActivity();
    this.getCampagins();
  }

  private getCampagins() {
    this.dashboardService.getCampaignsStatus().subscribe((res: any) => {
      for (let card of this.cardList) {
        card.counter = res.data[card.key];
        this.loading = false;
      }
    })
  }

  private getCustomersChart() {
    this.dashboardService.getCustomersChart().subscribe((res: any) => {
      this.setChart(res.data.reverse());
      this.loading = false;
    });
  }

  private setChart(chartData) {
    let labels = [];
    const data = [];

    for (const month of chartData) {
      labels.push(month.month_name);
      data.push(month.customers_total);
    }

    this.chart = {
      data: {
        datasets: [
          {
            data: data,
            backgroundColor: '#fdca52',
            borderColor: '#fdca52'
          }
        ],
        labels
      },
      options: {
        legend: {
          position: 'none'
        },
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true,
                maintainAspectRatio: true,
                stepSize: 1
              }
            }
          ]
        }
      }
    };
  }

  private getActivity() {
    this.dashboardService.getActivity().subscribe((res: any) => {
      this.activityList = res.data;
      this.loading = false;
    });
  }
}
