import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { CampaignsService } from 'src/app/pages/campaigns/services/campaigns.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { CustomersService } from 'src/app/pages/customers/services/customer.service';
import { DashboardService } from '../../services/dashboard.service';

@Component({
  selector: 'app-campaign-details',
  templateUrl: './campaign-details.component.html',
  styleUrls: ['./campaign-details.component.scss']
})
export class CampaignDetailsComponent implements OnInit {
  private campaignId: string;
  public campaign: any;
  public campaignForm: FormGroup;
  public paymentTypeList: any[];
  public campaignTypeList: any[];
  public subscriptionsList: any[];
  public loading = true;

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private dashboardService: DashboardService,
    private messageService: MessageService,
    private router: Router
  ) {
    this.campaignForm = this.formBuilder.group({
      campaign_type: [null, Validators.compose([Validators.required])],
      campaign_title: [null, Validators.compose([Validators.required])],
      customer_id: [null, Validators.compose([Validators.required])],
      campaign_status: [null, Validators.compose([Validators.required])],
      payment_type: [null, Validators.compose([Validators.required])],
      subscription_status: [null, Validators.compose([Validators.required])],
      manager_id: [null, Validators.compose([Validators.required])],
      admin_comment: [null]
    });

    this.paymentTypeList = [
      { value: 'invoice', label: 'Invoice' },
      { value: 'credit_card', label: 'Credit card' }
    ];

    this.campaignTypeList = [
      { value: 'facebook', label: 'Facebook ads' },
      { value: 'google', label: 'Google ads' },
      { value: 'call_rails', label: 'Call Rail ads' }
    ];

    this.subscriptionsList = [
      { value: '0', label: 'Active' },
      { value: '1', label: 'Disabled' }
    ];

    this.activatedRoute.paramMap.subscribe((res: any) => {
      this.campaignId = res.get('campaignId');
    });
  }

  ngOnInit() {
    this.getCampaign();
  }

  public createUpdateCampaign() {
    if (this.campaignForm.value.campaign_status === 'Active') {
      this.campaignForm.value.campaign_status = '1';
    } else if (this.campaignForm.value.campaign_status === 'Suspended') {
      this.campaignForm.value.campaign_status = '2';
    } else {
      this.campaignForm.value.campaign_status = '3';
    }

    this.dashboardService
      .updateCampaign(this.campaignForm.value, this.campaignId)
      .subscribe((res: any) => {
        this.router.navigate(['/dashboard']);
        this.messageService.add({
          severity: 'success',
          summary: res.message,
          closable: true
        });
      });
  }

  private getCampaign() {
    if (this.campaignId) {
      this.dashboardService
        .getCampaign(this.campaignId)
        .subscribe((res: any) => {
          res.data.campaign_status = res.data.campaign_status.toString();
          res.data.subscription_status = res.data.subscription_status.toString();
          if (res.data.campaign_status === '1') {
            res.data.campaign_status = 'Active';
          } else if (res.data.campaign_status === '2') {
            res.data.campaign_status = 'Suspended';
          } else {
            res.data.campaign_status = 'Cancelled';
          }

          if (res.data.payment_type === 'standard') {
            res.data.payment_type = 'invoice';
          }

          if (res.data.campaign_type === 'standard') {
            res.data.campaign_type = 'google';
          }

          this.campaignForm.patchValue(res.data);
          this.campaign = res.data;
          this.loading = false;
        });
    }
  }
}
