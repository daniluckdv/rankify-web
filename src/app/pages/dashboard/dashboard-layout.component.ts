import { Component } from '@angular/core';

@Component({
  selector: 'app-dashboard-client',
  template: '<router-outlet></router-outlet>'
})
export class DashboardLayoutComponent {}
