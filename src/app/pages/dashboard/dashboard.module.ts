import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './pages/dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { ChartModule } from 'primeng/chart';
import { DashboardAdminComponent } from './pages/dashboard-admin/dashboard-admin.component';
import { DashboardClientComponent } from './pages/dashboard-client/dashboard-client.component';
import { SharedModule } from '../../shared/shared.module';
import { DashboardService } from './services/dashboard.service';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { CampaignDetailsComponent } from './pages/campaign-details/campaign-details.component';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { DashboardLayoutComponent } from './dashboard-layout.component';

@NgModule({
  declarations: [
    DashboardComponent,
    DashboardAdminComponent,
    DashboardClientComponent,
    CampaignDetailsComponent,
    DashboardLayoutComponent
  ],
  imports: [
    CommonModule,
    ChartModule,
    DashboardRoutingModule,
    SharedModule,
    ButtonModule,
    InputTextModule,
    ReactiveFormsModule,
    DropdownModule,
    ProgressSpinnerModule    
  ],
  providers: [DashboardService]
})
export class DashboardModule {}
