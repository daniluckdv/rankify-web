import { Injectable } from '@angular/core';
import { ApiService } from '../../../core/services/api.service';

@Injectable()
export class DashboardService {
  constructor(private apiService: ApiService) {}

  public getCustomers() {
    return this.apiService.request('GET', 'dashboard/customers', {});
  }
  
  public getCampaignsStatus() {
    return this.apiService.request('GET', 'dashboard/campaignsStatus', {});
  }
  
  public getCustomersChart() {
    return this.apiService.request('GET', 'dashboard/dynamics', {});
  }
  
  public getActivity() {
    return this.apiService.request('GET', `activities?offset=0&limit=5`, {});
  }
  
  public getCampaigns() {
    return this.apiService.request('GET', 'dashboard/campaigns', {});
  }

  public getCampaign(campaignId: string) {
    return this.apiService.request('GET', `campaigns/${campaignId}`, {});
  }
  
  public getUserCampaigns(customerId: string) {
    return this.apiService.request('GET', `campaigns?offset=0&limit=5&${customerId}`, {});
  }
  
  public getUserInvoices(customerId: string) {
    return this.apiService.request('GET', `invoices?offset=0&limit=15&customer_id=${customerId}`, {});
  }

  public updateCampaign(body, id?: string) {
    body._method = 'patch';
    return this.apiService.request('POST', `campaigns/${id}`, body);
  }
}
