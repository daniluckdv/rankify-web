import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CampaignsService } from '../../services/campaigns.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-app-edit-campaign',
  templateUrl: './app-edit-campaign.component.html',
  styleUrls: ['./app-edit-campaign.component.scss']
})
export class AppEditCampaignComponent implements OnInit {
  public campaignId: string;
  public campaign: any;
  public addEditCampaignForm: FormGroup;
  public files: any[];
  public paymentTypeList: any[];
  public plansList: any[];
  public statusList: any[];
  public managersList = [];
  public customersList = [];
  public campaignsTypeList = [];
  public loading = true;

  constructor(
    private formBuilder: FormBuilder,
    private campaignsService: CampaignsService,
    private activatedRoute: ActivatedRoute,
    private messageService: MessageService,
    private userService: UserService,
    private router: Router
  ) {
    this.addEditCampaignForm = this.formBuilder.group({
      campaign_type: [null, Validators.compose([Validators.required])],
      campaign_title: [null, Validators.compose([Validators.required])],
      customer_id: [null, Validators.compose([Validators.required])],
      campaign_status: [null, Validators.compose([Validators.required])],
      payment_type: [null, Validators.compose([Validators.required])],
      manager_id: [null, Validators.compose([Validators.required])],
      plan: [null, Validators.compose([Validators.required])],
      admin_comment: [null]
    });

    this.paymentTypeList = [
      { value: 'invoice', label: 'Invoice' },
      { value: 'credit_card', label: 'Credit card' }
    ];

    this.statusList = [
      { value: '1', label: 'Active' },
      { value: '2', label: 'Suspended' },
      { value: '3', label: 'Cancelled' }
    ];

    this.campaignsTypeList = [
      { value: 'google', label: 'Google ads' },
      { value: 'facebook', label: 'Facebook ads' },
      { value: 'call', label: 'Call Rails ads' }
    ];

    this.activatedRoute.paramMap.subscribe(
      res => (this.campaignId = res.get('id'))
    );
  }

  ngOnInit() {
    if (this.userService.user.role !== 'customer') {
      this.getManagers();
      this.getCustomers();
    }

    this.getCampaign();
    this.getPlans();
  }

  public createUpdateCampaign() {
    if (this.campaignId) {
      this.campaignsService
        .createCampaign(this.addEditCampaignForm.value, this.campaignId)
        .subscribe((res: any) => {
          this.router.navigate(['/campaigns']);
          this.messageService.add({
            severity: 'success',
            summary: res.message,
            closable: true
          });
          this.addEditCampaignForm.reset();
        });
    } else {
      this.campaignsService
        .createCampaign(this.addEditCampaignForm.value)
        .subscribe((res: any) => {
          this.router.navigate(['/campaigns']);

          this.messageService.add({
            severity: 'success',
            summary: res.message,
            closable: true
          });

          this.addEditCampaignForm.reset();
        });
    }
  }

  public selectAvatar(files) {
    let formData = new FormData();
    formData.append('photo', files[0]);

    this.campaignsService
      .addCampaignPhoto(formData, this.campaignId)
      .subscribe((res: any) => {
        this.messageService.add({
          severity: 'success',
          summary: res.message,
          closable: true
        });
      });

    return (this.files = files);
  }

  public deletePhoto() {
    if (this.files || this.campaign.photo_path) {
      this.campaignsService
        .deleteCampaignPhoto(this.campaignId)
        .subscribe((res: any) => {
          this.files = null;
          this.campaign.photo_path = null;

          this.messageService.add({
            severity: 'success',
            summary: res.message,
            closable: true
          });
        });
    }
  }

  private getCustomers() {
    this.campaignsService.getCustomers().subscribe((res: any) => {
      for (const customer of res.data) {
        this.customersList.push({
          label: customer.first_name + ' ' + customer.last_name,
          value: customer.id
        });

        this.loading = false;
      }
    });
  }

  private getCampaign() {
    if (this.campaignId) {
      this.campaignsService
        .getCampaign(this.campaignId)
        .subscribe((res: any) => {
          res.data.campaign_status = res.data.campaign_status.toString();
          res.data.subscription_status = res.data.subscription_status.toString();

          this.customersList.push({
            label: res.data.customer_name,
            value: res.data.customer_id
          });

          this.managersList.push({
            label: res.data.manager_name,
            value: res.data.manager_id
          });

          if (res.data.payment_type === 'standard') {
            res.data.payment_type = 'invoice';
          }

          if (res.data.campaign_type === 'standard') {
            res.data.campaign_type = 'google';
          }

          this.addEditCampaignForm.patchValue(res.data);
          this.campaign = res.data;
          this.loading = false;
        });
    }
  }

  private getManagers() {
    this.campaignsService.getManagers().subscribe((res: any) => {
      for (const manager of res.data) {
        this.managersList.push({
          label: manager.first_name + ' ' + manager.last_name,
          value: manager.id
        });

        this.addEditCampaignForm.patchValue(res.data);
        this.loading = false;
      }
    });
  }

  private getPlans() {
    this.campaignsService.getPlans().subscribe((res: any) => {
      this.plansList = res.data.map(plan => {
        return {
          label: `${plan.title} ($${plan.price}/month)`,
          value: plan.name
        };
      })
    });
  }
}
