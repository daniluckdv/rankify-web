import { Component, OnInit, OnDestroy } from '@angular/core';
import { CampaignsService } from '../../../services/campaigns.service';
import { ActivatedRoute } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { HeaderService } from 'src/app/shared/components/header/header.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-campaigns-admin',
  templateUrl: './campaigns-admin.component.html',
  styleUrls: ['./campaigns-admin.component.scss']
})
export class CampaignsAdminComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  private draggedCampaign: any;
  private chosenCampaignType: string;

  public droppedArea: string;
  public campaignType: string;
  public loading = true;
  public total = 10;
  public view = 'table';
  public viewIcon = 'pi pi-list';
  public campaignsCols: any[];
  public campaignsTable: any[];
  public campaignsList: CampaignsList = {
    activeCampaigns: [],
    suspendedCampaigns: [],
    cancelledCampaigns: []
  };

  constructor(
    private campaignsService: CampaignsService,
    private confirmationService: ConfirmationService,
    private activatedRoute: ActivatedRoute,
    private headerService: HeaderService
  ) {
    const activatedRouteSubscribe = this.activatedRoute.paramMap.subscribe(
      res => {
        this.campaignType = res.get('type');
      }
    );
    this.subscriptions.push(activatedRouteSubscribe);

    this.campaignsCols = [
      { header: 'Campaign Title', field: 'campaign_title' },
      { header: 'Created', field: 'created_at' },
      { header: 'Type', field: 'campaign_type' },
      { header: 'Plan', field: 'plan' },
      { header: 'Status', field: 'campaign_status' }
    ];
  }

  ngOnInit() {
    this.getDataFromSearch();
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
    this.headerService.setFindedData(null);
    this.headerService.changeHeaderData(null);
  }

  public changeView() {
    this.view === 'table' ? (this.view = 'list') : (this.view = 'table');
    this.viewIcon === 'pi pi-table'
      ? (this.viewIcon = 'pi pi-list')
      : (this.viewIcon = 'pi pi-table');
  }

  public onDragStart(chosenCampaignType, draggedCampaign) {
    this.chosenCampaignType = chosenCampaignType;
    this.draggedCampaign = draggedCampaign;
  }

  public onDragEnd() {
    this.droppedArea = null;
  }

  public onDragEnter(area) {
    this.droppedArea = area;
  }

  public drop(draggedTo) {
    if (this.chosenCampaignType === draggedTo) {
      return;
    }

    this.confirmationService.confirm({
      message: 'Do you want to change the status of this campaign?',
      accept: () => {
        switch (draggedTo) {
          case 'active':
            const toggleActiveCampaignStatusSubscribe = this.campaignsService
              .toggleCampaignStatus(this.draggedCampaign.id, '1')
              .subscribe(() => {
                this.campaignsList.activeCampaigns.push(this.draggedCampaign);
                this.removeDraggedCampaign();
              });

            this.subscriptions.push(toggleActiveCampaignStatusSubscribe);
            break;
          case 'suspended':
            const toggleSuspendedCampaignStatusSubscribe = this.campaignsService
              .toggleCampaignStatus(this.draggedCampaign.id, '2')
              .subscribe(() => {
                this.campaignsList.suspendedCampaigns.push(
                  this.draggedCampaign
                );
                this.removeDraggedCampaign();
              });

            this.subscriptions.push(toggleSuspendedCampaignStatusSubscribe);
            break;
          case 'cancelled':
            const toggleCancelledCampaignStatusSubscribe = this.campaignsService
              .toggleCampaignStatus(this.draggedCampaign.id, '3')
              .subscribe(() => {
                this.campaignsList.cancelledCampaigns.push(
                  this.draggedCampaign
                );
                this.removeDraggedCampaign();
              });
            this.subscriptions.push(toggleCancelledCampaignStatusSubscribe);
            break;
          default:
            break;
        }
      }
    });
  }

  private removeDraggedCampaign() {
    if (this.chosenCampaignType === 'active') {
      this.campaignsList.activeCampaigns = this.campaignsList.activeCampaigns.filter(
        (item: any) => item.id !== this.draggedCampaign.id
      );
    }

    if (this.chosenCampaignType === 'suspended') {
      this.campaignsList.suspendedCampaigns = this.campaignsList.suspendedCampaigns.filter(
        (item: any) => item.id !== this.draggedCampaign.id
      );
    }

    if (this.chosenCampaignType === 'cancelled') {
      this.campaignsList.cancelledCampaigns = this.campaignsList.cancelledCampaigns.filter(
        (item: any) => item.id !== this.draggedCampaign.id
      );
    }

    this.draggedCampaign = null;
    this.chosenCampaignType = null;
  }

  private parseCampaigns(data) {
    data.forEach(campaign => {
      if (
        campaign.campaign_type === 'standard' ||
        campaign.campaign_type === 'google'
      ) {
        campaign.campaign_type = 'Google Ads';
      }

      if (campaign.campaign_type === 'facebook') {
        campaign.campaign_type = 'Facebook Ads';
      }

      if (campaign.campaign_type === 'call_rails') {
        campaign.campaign_type = 'Call Rail Ads';
      }

      if (campaign.campaign_status === 3 || campaign.campaign_status === 0) {
        campaign.campaign_status = 'Cancelled';
        this.campaignsList.cancelledCampaigns.push(campaign);
      } else if (campaign.campaign_status === 2) {
        campaign.campaign_status = 'Suspended';
        this.campaignsList.suspendedCampaigns.push(campaign);
      } else if (campaign.campaign_status === 1) {
        campaign.campaign_status = 'Active';
        this.campaignsList.activeCampaigns.push(campaign);
      }
    });

    this.campaignsTable = data;
    this.loading = false;
  }

  private getCampaignsFromAPI() {
    const getCampaignsSubscribe = this.campaignsService
      .getCampaigns()
      .subscribe((res: any) => {
        this.total = res.meta.total;
        this.parseCampaigns(res.data);
      });

    this.subscriptions.push(getCampaignsSubscribe);
  }

  private getDataFromSearch() {
    this.loading = true;

    const getFindedDataSubscribe = this.headerService.getFindedData.subscribe(
      res => {
        if (res) {
          this.total = res.meta.total;
          this.campaignsTable = [];
          this.campaignsList = {
            activeCampaigns: [],
            suspendedCampaigns: [],
            cancelledCampaigns: []
          };
          this.parseCampaigns(res);
        } else {
          this.getCampaignsFromAPI();
        }
      }
    );

    this.subscriptions.push(getFindedDataSubscribe);
  }
}
