import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../core/services/user.service';
import { HeaderService } from 'src/app/shared/components/header/header.service';

@Component({
  selector: 'app-campaigns-list',
  templateUrl: './campaigns-list.component.html'
})
export class CampaignsListComponent implements OnInit {
  public user: any;

  constructor(
    private userService: UserService,
    private headerService: HeaderService
  ) {
    this.user = this.userService.user;
  }

  ngOnInit() {
    this.headerService.changeHeaderData('campaigns');
  }
}
