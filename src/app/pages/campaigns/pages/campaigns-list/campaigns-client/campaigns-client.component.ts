import { Component, OnInit, OnDestroy } from '@angular/core';
import { DashboardService } from 'src/app/pages/dashboard/services/dashboard.service';
import { UserService } from 'src/app/core/services/user.service';
import { Subscription } from 'rxjs';
import { HeaderService } from 'src/app/shared/components/header/header.service';

@Component({
  selector: 'app-campaigns-client',
  templateUrl: './campaigns-client.component.html',
  styleUrls: ['./campaigns-client.component.scss']
})
export class CampaignsClientComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  public campaignsCols: any[];
  public campaignsList = [];
  public loading = true;
  public total = 10;

  constructor(
    private dashboardService: DashboardService,
    private userService: UserService,
    private headerService: HeaderService
  ) {
    this.campaignsCols = [
      { header: 'Campaign Title', field: 'campaign_title' },
      { header: 'Created', field: 'created_at' },
      { header: 'Type', field: 'campaign_type' },
      { header: 'Status', field: 'campaign_status' }
    ];
  }

  ngOnInit() {
    this.getDataFromSearch();
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
    this.headerService.setFindedData(null);
    this.headerService.changeHeaderData(null);
  }

  private parseCampaings(data) {
    for (const campaign of data) {
      if (campaign.campaign_status === 1) {
        campaign.campaign_status = 'Active';
      } else if (campaign.campaign_status === 3) {
        campaign.campaign_status = 'Cancelled';
      } else if (campaign.campaign_status === 2) {
        campaign.campaign_status = 'Suspended';
      }

      if (
        campaign.campaign_type === 'standard' ||
        campaign.campaign_type === 'google'
      ) {
        campaign.campaign_type = 'Google Ads';
      }

      if (campaign.campaign_type === 'facebook') {
        campaign.campaign_type = 'Facebook Ads';
      }

      if (campaign.campaign_type === 'call_rails') {
        campaign.campaign_type = 'Call Rail Ads';
      }
    }
    this.campaignsList = data;
    this.loading = false;
  }

  private getCampaignsFromAPI() {
    const getUserCampaignsSubscribe = this.dashboardService
      .getUserCampaigns(this.userService.user.id)
      .subscribe((res: any) => {
        this.total = res.meta.total;
        this.parseCampaings(res.data);
      });

    this.subscriptions.push(getUserCampaignsSubscribe);
  }

  private getDataFromSearch() {
    this.loading = true;

    const getFindedDataSubscribe = this.headerService.getFindedData.subscribe(
      res => {
        if (res) {
          this.total = res.meta.total;
          this.campaignsList = [];
          this.parseCampaings(res);
        } else {
          this.getCampaignsFromAPI();
        }
      }
    );

    this.subscriptions.push(getFindedDataSubscribe);
  }
}
