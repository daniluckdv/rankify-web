import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CampaignsComponent } from './campaigns.component';
import { CampaignsRoutingModule } from './campaigns-routing.module';
import { AppEditCampaignComponent } from './pages/app-edit-campaign/app-edit-campaign.component';
import { InputTextModule } from 'primeng/inputtext';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
import { CampaignsAdminComponent } from './pages/campaigns-list/campaigns-admin/campaigns-admin.component';
import { CampaignsClientComponent } from './pages/campaigns-list/campaigns-client/campaigns-client.component';
import { CampaignsListComponent } from './pages/campaigns-list/campaigns-list.component';
import { SharedModule } from '../../shared/shared.module';
import { ToastModule } from 'primeng/toast';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { DashboardModule } from '../dashboard/dashboard.module';
import { DragDropModule } from 'primeng/dragdrop';
import { ConfirmationService } from 'primeng/api';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { FileUploadModule } from 'primeng/fileupload';

@NgModule({
  declarations: [
    CampaignsComponent,
    AppEditCampaignComponent,
    CampaignsAdminComponent,
    CampaignsClientComponent,
    CampaignsListComponent
  ],
  imports: [
    CommonModule,
    CampaignsRoutingModule,
    DashboardModule,
    ButtonModule,
    InputTextModule,
    ReactiveFormsModule,
    CalendarModule,
    DropdownModule,
    SharedModule,
    ToastModule,
    DragDropModule,
    ProgressSpinnerModule,
    ConfirmDialogModule,
    FileUploadModule
  ],
  providers: [ConfirmationService]
})
export class CampaignsModule {}
