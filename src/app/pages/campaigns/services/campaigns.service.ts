import { Injectable } from '@angular/core';
import { ApiService } from '../../../core/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class CampaignsService {
  constructor(private apiService: ApiService) {}

  public getCampaigns() {
    return this.apiService.request('GET', 'campaigns?offset=0&limit=20', {});
  }
  
  public getCampaign(id: string) {
    return this.apiService.request('GET', `campaigns/${id}`, {});
  }

  public toggleCampaignStatus(id, campaign_status: string) {
    return this.apiService.request('POST', `campaigns/${id}/toggleStatus`, { campaign_status });
  }
  
  public createCampaign(body: ICreateUpdateCampaign, id?: string) {
    if (id) {
      body._method = 'patch';
      return this.apiService.request('POST', `campaigns/${id}`, body);
    } else {
      return this.apiService.request('POST', 'campaigns', body);
    }
  }

  public getCustomers() {
    return this.apiService.request('GET', 'users?level=customer', {});
  }
  
  public getManagers() {
    return this.apiService.request('GET', 'users?level=manager', {});
  }

  public getPlans() {
    return this.apiService.request('GET', 'plans');
  }

  public addCampaignPhoto(files, id?: string) {
    if (id) {
      return this.apiService.request('POST', `campaigns/${id}/photo`, files);
    } else {
      return this.apiService.request('POST', 'campaigns/photo', files);
    }
  }

  public deleteCampaignPhoto(id: string) {
    return this.apiService.request('DELETE', `campaigns/${id}/photo`, {});
  }
}
