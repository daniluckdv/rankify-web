import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CampaignsComponent } from './campaigns.component';
import { AppEditCampaignComponent } from './pages/app-edit-campaign/app-edit-campaign.component';
import { CampaignsListComponent } from './pages/campaigns-list/campaigns-list.component';
import { CampaignDetailsComponent } from '../dashboard/pages/campaign-details/campaign-details.component';

const routes: Routes = [
  {
    path: '',
    component: CampaignsComponent,
    children: [
      { path: '', component: CampaignsListComponent },
      { path: 'add', component: AppEditCampaignComponent },
      { path: ':id', component: AppEditCampaignComponent },
      { path: ':campaignId', component: CampaignDetailsComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampaignsRoutingModule {}
