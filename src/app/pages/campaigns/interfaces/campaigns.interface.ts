interface CampaignsList {
  activeCampaigns: object[];
  suspendedCampaigns: object[];
  cancelledCampaigns: object[];
}

interface ICreateUpdateCampaign {
  readonly campaign_type: string;
  readonly campaign_title: string;
  readonly customer_id: string;
  readonly campaign_status: string;
  readonly payment_type: string;
  readonly subscription_status: string;
  readonly manager_id: string;
  readonly admin_comment?: string;
  _method?: string;
}
