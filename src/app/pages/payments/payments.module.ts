import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentsComponent } from './payments.component';
import { PaymentsRoutingModule } from './payments-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { ButtonModule } from 'primeng/button';
import { PaymentsAdminComponent } from './pages/payments-admin/payments-admin.component';
import { PaymentsClientComponent } from './pages/payments-client/payments-client.component';
import { PaymentsService } from './services/payments.service';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { AddEditPaymentComponent } from './pages/add-edit-payment/add-edit-payment.component';
import { PaymentsListComponent } from './pages/payments-list/payments-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';

@NgModule({
  declarations: [
    PaymentsComponent,
    PaymentsAdminComponent,
    PaymentsClientComponent,
    AddEditPaymentComponent,
    PaymentsListComponent
  ],
  imports: [
    ToastModule,
    CommonModule,
    ButtonModule,
    SharedModule,
    ReactiveFormsModule,
    DropdownModule,
    InputTextModule,
    ProgressSpinnerModule,
    PaymentsRoutingModule
  ],
  providers: [MessageService, PaymentsService]
})
export class PaymentsModule {}
