import { Injectable } from '@angular/core';
import { ApiService } from '../../../core/services/api.service';

@Injectable()
export class PaymentsService {
  constructor(private apiService: ApiService) {}

  public getInvoices() {
    return this.apiService.request('GET', 'invoices?offset=0&limit=10', {});
  }

  public getInvoice(id: string) {
    return this.apiService.request('GET', `invoices/${id}`, {});
  }
  
  public getCampaigns() {
    return this.apiService.request('GET', `campaigns?offset=0&limit=1000`, {});
  }

  public createUpdateInvoice(body: ICreateUpdateInvoice, id?: string) {
    if (id) {
      body._method = 'patch';
      return this.apiService.request('POST', `invoices/${id}`, body);
    } else {
      return this.apiService.request('POST', 'invoices', body);
    }
  }

  public deleteInvoice(id: string) {
    return this.apiService.request('DELETE', `invoices/${id}`, {});
  }
}
