import { Component } from '@angular/core';

@Component({
  selector: 'app-payments',
  template: '<router-outlet></router-outlet><p-toast position="top-right"></p-toast>',
})
export class PaymentsComponent {
}
