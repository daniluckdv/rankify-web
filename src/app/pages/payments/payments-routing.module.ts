import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaymentsComponent } from './payments.component';
import { AddEditPaymentComponent } from './pages/add-edit-payment/add-edit-payment.component';
import { PaymentsListComponent } from './pages/payments-list/payments-list.component';

const routes: Routes = [
  {
    path: '',
    component: PaymentsComponent,
    children: [
      { path: '', component: PaymentsListComponent },
      { path: 'create', component: AddEditPaymentComponent },
      { path: ':id', component: AddEditPaymentComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentsRoutingModule {}
