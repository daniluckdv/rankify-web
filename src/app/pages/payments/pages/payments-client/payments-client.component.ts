import { Component, OnInit, OnDestroy } from '@angular/core';
import { PaymentsService } from '../../services/payments.service';
import { Subscription } from 'rxjs';
import { HeaderService } from 'src/app/shared/components/header/header.service';

@Component({
  selector: 'app-payments-client',
  templateUrl: './payments-client.component.html',
  styleUrls: ['./payments-client.component.scss']
})
export class PaymentsClientComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  public tableHeaders: any[];
  public paymentsList = [];
  public loading = true;

  constructor(
    private paymentsService: PaymentsService,
    private headerService: HeaderService
  ) {
    this.tableHeaders = [
      { header: 'Service', field: 'campaign_type' },
      { header: 'Date of payment', field: 'created_at' },
      { header: 'Payment method', field: 'payment_type' },
      { header: 'Amount', field: 'amount' },
      { header: 'Status', field: 'invoice_status' }
    ];
  }

  ngOnInit() {
    this.getDataFromSearch();
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
    this.headerService.setFindedData(null);
    this.headerService.changeHeaderData(null);
  }

  private parsePayments(data) {
    data.forEach(payment => {
      payment.amount = '$ ' + payment.amount.toFixed(2).toString();
      payment.created_at = payment.payment_date;
      payment.campaign_type =
        payment.campaign_type + '. ID: ' + payment.campaign_id;

      if (payment.campaign_type === 'standard') {
        payment.campaign_type = 'Google';
      }

      if (payment.payment_type === 'standard') {
        payment.payment_type = 'Invoice';
      }
      if (payment.invoice_status === 1) {
        payment.invoice_status = 'Paid';
      } else {
        payment.invoice_status = 'Overdue';
      }
    });

    this.paymentsList = data;
    this.loading = false;
  }

  private getPaymentsFromAPI() {
    const getInvoicesSubscribe = this.paymentsService
      .getInvoices()
      .subscribe((res: any) => {
        this.parsePayments(res.data);
      });

    this.subscriptions.push(getInvoicesSubscribe);
  }

  private getDataFromSearch() {
    this.loading = true;
    const getSearchedDataSubscribe = this.headerService.getFindedData.subscribe(
      res => {
        if (res) {
          this.paymentsList = [];
          this.parsePayments(res);
        } else {
          this.getPaymentsFromAPI();
        }
      }
    );

    this.subscriptions.push(getSearchedDataSubscribe);
  }
}
