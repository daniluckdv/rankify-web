import { Component, OnInit } from '@angular/core';
import { PaymentsService } from '../../services/payments.service';
import { MessageService } from 'primeng/api';
import { HeaderService } from 'src/app/shared/components/header/header.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-payments-admin',
  templateUrl: './payments-admin.component.html',
  styleUrls: ['./payments-admin.component.scss']
})
export class PaymentsAdminComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  public tableHeaders: any[];
  public paymentsList: any[];
  public loading = true;
  public total = 10;

  constructor(
    private paymentsService: PaymentsService,
    private messageService: MessageService,
    private headerService: HeaderService
  ) {
    this.tableHeaders = [
      { header: 'Client Name', field: 'customer_name' },
      { header: 'Campaign', field: 'campaign_type' },
      { header: 'Date of payment', field: 'created_at' },
      { header: 'Payment method', field: 'payment_type' },
      { header: 'Amount ($)', field: 'amount' },
      { header: 'Status', field: 'invoice_status' }
    ];
  }

  ngOnInit() {
    this.getDataFromSearch();
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
    this.headerService.setFindedData(null);
    this.headerService.changeHeaderData(null);
  }

  public delete(id: string) {
    const deleteInvoiceSubscribe = this.paymentsService
      .deleteInvoice(id)
      .subscribe((res: any) => {
        this.paymentsList = this.paymentsList.filter(
          payment => payment.id !== id
        );
        this.messageService.add({
          severity: 'success',
          summary: res.message,
          closable: true
        });
      });

    this.subscriptions.push(deleteInvoiceSubscribe);
  }

  private parsePayments(data) {
    this.loading = true;
    data.forEach(payment => {
      payment.amount = '$ ' + payment.amount.toFixed(2).toString();
      payment.created_at = payment.payment_date;

      if (payment.campaign_type === 'standard') {
        payment.campaign_type = 'Google';
      }

      if (payment.payment_type === 'standard') {
        payment.payment_type = 'Invoice';
      }

      if (payment.invoice_status === 1) {
        payment.invoice_status = 'Paid';
      } else if (payment.invoice_status === 2) {
        payment.invoice_status = 'Overdue';
      } else if (payment.invoice_status === 3 || payment.invoice_status === 0) {
        payment.invoice_status = 'Unpaid';
      }

      this.paymentsList = data;
    });
    
    this.loading = false;
  }

  private getPaymentsFromAPI() {
    const getInvoicesSubscribe = this.paymentsService
      .getInvoices()
      .subscribe((res: any) => {
        this.total = res.meta.total;
        this.parsePayments(res.data);
      });

    this.subscriptions.push(getInvoicesSubscribe);
  }

  private getDataFromSearch() {
    const getSearchedDataSubscribe = this.headerService.getFindedData.subscribe(
      res => {
        if (res) {
          this.total = res.meta.total;
          this.paymentsList = [];
          this.parsePayments(res.data);
        } else {
          this.getPaymentsFromAPI();
        }
      }
    );

    this.subscriptions.push(getSearchedDataSubscribe);
  }
}
