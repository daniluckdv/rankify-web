import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/services/user.service';
import { HeaderService } from 'src/app/shared/components/header/header.service';

@Component({
  selector: 'app-payments-list',
  templateUrl: './payments-list.component.html',
  styleUrls: ['./payments-list.component.scss']
})
export class PaymentsListComponent implements OnInit {
  public user: any;

  constructor(
    private readonly userService: UserService,
    private readonly headerService: HeaderService
  ) {
    this.user = this.userService.user;
  }

  ngOnInit() {
    this.headerService.changeHeaderData('invoices');
  }
}
