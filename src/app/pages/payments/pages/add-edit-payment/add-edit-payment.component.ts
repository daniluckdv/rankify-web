import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PaymentsService } from '../../services/payments.service';
import { MessageService } from 'primeng/api';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-add-edit-payment',
  templateUrl: './add-edit-payment.component.html',
  styleUrls: ['./add-edit-payment.component.scss']
})
export class AddEditPaymentComponent implements OnInit {
  public paymentId: string;
  public addEditPaymentForm: FormGroup;
  public statusList = [];
  public campaignsList = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private paymentsService: PaymentsService,
    private messageService: MessageService,
    private userService: UserService,
    private router: Router
  ) {
    this.activatedRoute.paramMap.subscribe(
      res => (this.paymentId = res.get('id'))
    );

    this.addEditPaymentForm = this.formBuilder.group({
      campaign_id: [null, Validators.compose([Validators.required])],
      amount: [null, Validators.compose([Validators.required])],
      invoice_status: [null, Validators.compose([Validators.required])],
      payment_date: [null]
    });

    this.statusList = [
      { label: 'Active', value: '1' },
      { label: 'Overdue', value: '2' },
      { label: 'Unpaid', value: '3' }
    ];
  }

  ngOnInit() {
    this.getPayment();
    this.getCampaigns();
  }

  public addEditPayment() {
    // const currentDate = new Date(Date.now()).toLocaleString();
    this.addEditPaymentForm.value.payment_date = '2019-05-15 16:00:00';

    this.paymentsService
      .createUpdateInvoice(this.addEditPaymentForm.value, this.paymentId)
      .subscribe((res: any) => {
        this.router.navigate(['/payments']);

        this.messageService.add({
          severity: 'success',
          summary: res.message,
          closable: true
        });
      });
  }

  private getPayment() {
    if (this.paymentId) {
      this.paymentsService.getInvoice(this.paymentId).subscribe((res: any) => {
        res.data.invoice_status = res.data.invoice_status.toString();
        res.data.campaign_id = res.data.campaign_id.toString();
        this.addEditPaymentForm.patchValue(res.data);
      });
    }
  }

  private getCampaigns() {
    this.paymentsService
      .getCampaigns()
      .subscribe((res: any) => {
        for (const campaign of res.data)
          this.campaignsList.push({
            value: campaign.id.toString(),
            label: `${campaign.customer_name}, ${campaign.campaign_type} (${
              campaign.id
            })`
          });

        for (let campaign of this.campaignsList) {
          if (campaign.id === res.data.campaign_id) {
            this.addEditPaymentForm.get('campaign_id').patchValue(campaign.id);
          }
        }
      });
  }
}
