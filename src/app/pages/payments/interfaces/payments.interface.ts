interface ICreateUpdateInvoice {
  readonly campaign_id: string;
  readonly payment_date: string;
  readonly amount: string;
  readonly invoice: string;
  _method?: string;
}
