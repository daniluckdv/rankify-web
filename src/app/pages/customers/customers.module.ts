import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomersRoutingModule } from './customers-routing.module';
import { CustomersComponent } from './pages/customers.component';
import { SharedModule } from '../../shared/shared.module';
import { ButtonModule } from 'primeng/button';
import { AddEditCustomerComponent } from './pages/add-edit-customer/add-edit-customer.component';
import { CustomersListComponent } from './pages/customers-list/customers-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { DropdownModule } from 'primeng/dropdown';
import { FileUploadModule } from 'primeng/fileupload';
import { CustomersService } from './services/customer.service';
import { ToastModule } from 'primeng/toast';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { SupportModule } from '../support/support.module';

@NgModule({
  declarations: [
    CustomersComponent,
    AddEditCustomerComponent,
    CustomersListComponent
  ],
  imports: [
    CommonModule,
    ButtonModule,
    InputTextModule,
    ReactiveFormsModule,
    CustomersRoutingModule,
    SharedModule,
    InputTextareaModule,
    DropdownModule,
    FileUploadModule,
    ToastModule,
    ProgressSpinnerModule,
    DropdownModule,
    SupportModule
  ],
  providers: [CustomersService]
})
export class CustomersModule {}
