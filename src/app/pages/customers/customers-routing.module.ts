import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomersComponent } from './pages/customers.component';
import { AddEditCustomerComponent } from './pages/add-edit-customer/add-edit-customer.component';
import { CustomersListComponent } from './pages/customers-list/customers-list.component';
import { TicketsComponent } from '../support/pages/tickets/tickets.component';
import { TicketComponent } from '../support/pages/ticket/ticket.component';

const routes: Routes = [
  {
    path: '',
    component: CustomersComponent,
    children: [
      { path: '', component: CustomersListComponent },
      { path: 'add', component: AddEditCustomerComponent },
      { path: ':id', component: AddEditCustomerComponent },
      { path: ':id/support-tickets', component: TicketsComponent },
      { path: ':id/support-tickets/:ticketId', component: TicketComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomersRoutingModule {}
