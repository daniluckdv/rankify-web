import { Injectable } from '@angular/core';
import { ApiService } from '../../../core/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {
  constructor(private apiService: ApiService) {}

  public getCustomers() {
    return this.apiService.request('GET', 'users?level=customer', {});
  }

  public getCustomer(id: string) {
    return this.apiService.request('GET', `users/${id}`, {});
  }

  public getPlans() {
    return this.apiService.request('GET', 'plans');
  }

  public createUpdateUser(body: ICreateUpdateCustomer, id?: string) {
    if (id) {
      body._method = 'patch';
      return this.apiService.request('POST', `users/${id}`, body);
    } else {
      return this.apiService.request('POST', 'users', body);
    }
  }
  
  public pauseCampaigns(customer_id: string) {
    return this.apiService.request('POST', 'campaigns/pause', { customer_id });
  }

  public addUserPhoto(files, id?: string) {
    if (id) {
      return this.apiService.request('POST', `users/${id}/photo`, files);
    } else {
      return this.apiService.request('POST', 'users/photo', files);
    }
  }

  public deleteUser(id: string) {
    return this.apiService.request('DELETE', `users/${id}`, {});
  }

  public deleteUserPhoto(id: string) {
    return this.apiService.request('DELETE', `users/${id}/photo`, {});
  }
}
