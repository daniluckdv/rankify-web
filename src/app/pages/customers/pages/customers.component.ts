import { Component } from '@angular/core';

@Component({
  selector: 'app-customers',
  template: '<router-outlet></router-outlet><p-toast position="top-right"></p-toast>'
})

export class CustomersComponent {
  constructor() {}
}
