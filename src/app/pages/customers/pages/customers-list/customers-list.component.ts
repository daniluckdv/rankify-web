import { Component, OnInit, OnDestroy } from '@angular/core';
import { CustomersService } from '../../services/customer.service';
import { MessageService } from 'primeng/api';
import { HeaderService } from 'src/app/shared/components/header/header.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-customers-list',
  templateUrl: './customers-list.component.html',
  styleUrls: ['./customers-list.component.scss']
})
export class CustomersListComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  public tableHeaders: any[];
  public customersList = [];
  public loading = true;
  public total = 10;

  constructor(
    private customersService: CustomersService,
    private messageService: MessageService,
    private headerService: HeaderService
  ) {
    this.tableHeaders = [
      { header: 'Client Name', field: 'fullName' },
      { header: 'User type', field: 'level' },
      { header: 'Business name', field: 'business_name' },
      { header: 'Created', field: 'created_at' },
      { header: 'Support Tickets', field: 'tickets' },
      { header: 'Status', field: 'status' }
    ];
  }

  ngOnInit() {
    this.headerService.changeHeaderData('users');
    this.getDataFromSearch();
  }

  ngOnDestroy() {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
    this.headerService.setFindedData(null);
    this.headerService.changeHeaderData(null);
  }

  public deleteCustomer(id: string) {
    const deleteUserSubscribe = this.customersService
      .deleteUser(id)
      .subscribe((res: any) => {
        this.customersList = this.customersList.filter(
          payment => payment.id !== id
        );

        this.messageService.add({
          severity: 'success',
          summary: res.message,
          closable: true
        });
      });

    this.subscriptions.push(deleteUserSubscribe);
  }

  private parseCustomers(data) {
    for (let customer of data) {
      customer.fullName = customer.first_name + ' ' + customer.last_name;
      if (customer.status === 1) {
        customer.status = 'Active';
      } else if (customer.status === 3) {
        customer.status = 'Disabled';
      } else if (customer.status === 2) {
        customer.status = 'Suspended';
      }
    }

    this.customersList = data;
    this.loading = false;
  }

  private getCustomersFromAPI() {
    const getCustomersSubscribe = this.customersService
      .getCustomers()
      .subscribe((res: any) => {
        this.total = res.meta.total;
        this.parseCustomers(res.data);
      });

    this.subscriptions.push(getCustomersSubscribe);
  }

  private getDataFromSearch() {
    const getSearchedData = this.headerService.getFindedData.subscribe(res => {
      if (res) {
        this.total = res.meta.total;
        this.customersList = [];
        this.parseCustomers(res);
      } else {
        this.getCustomersFromAPI();
      }
    });

    this.subscriptions.push(getSearchedData);
  }
}
