import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomersService } from '../../services/customer.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { UserService } from '../../../../core/services/user.service';

@Component({
  selector: 'app-add-edit-customer',
  templateUrl: './add-edit-customer.component.html',
  styleUrls: ['./add-edit-customer.component.scss']
})
export class AddEditCustomerComponent implements OnInit {
  public customerId: string;
  public customer: any = {};
  public files: any[];
  public addEditCustomerForm: FormGroup;
  public statusList: any[];
  public levelsList: any[];
  public plansList: any[];
  public loading = true;

  constructor(
    private formBuilder: FormBuilder,
    private customerService: CustomersService,
    private activatedRoute: ActivatedRoute,
    private messageService: MessageService,
    private userService: UserService,
    private router: Router
  ) {
    this.activatedRoute.paramMap.subscribe(
      res => (this.customerId = res.get('id'))
    );

    this.addEditCustomerForm = this.formBuilder.group({
      first_name: [null, Validators.compose([Validators.required])],
      last_name: [null, Validators.compose([Validators.required])],
      username: [null, Validators.compose([Validators.required])],
      business_name: [null, Validators.compose([Validators.required])],
      email: [
        null,
        Validators.compose([Validators.required, Validators.email])
      ],
      phone: [null, Validators.compose([Validators.required])],
      status: [null, Validators.compose([Validators.required])],
      plan: [null],
      comment: [null],
      level: [null],
      password: [null]
    });

    this.statusList = [
      { label: 'Active', value: '1' },
      { label: 'Suspended', value: '2' },
      { label: 'Cancelled', value: '3' }
    ];

    this.levelsList = [
      { value: 'customer', label: 'Customer' },
      { value: 'manager', label: 'Manager' }
    ];

    if (this.userService.user.role === 'admin') {
      this.levelsList.push({
        value: 'admin',
        label: 'Admin'
      });
    }
  }

  ngOnInit() {
    this.getCustomer();
    this.getPlans();
  }

  public addEditCustomer() {
    if (!this.addEditCustomerForm.value.password) {
      this.addEditCustomerForm.removeControl('password');
    }

    this.customerService
      .createUpdateUser(this.addEditCustomerForm.value, this.customerId)
      .subscribe((res: any) => {
        this.router.navigate(['/customers']);
        this.messageService.add({
          severity: 'success',
          summary: res.message,
          closable: true
        });
      });
  }

  public selectAvatar(files) {
    let formData = new FormData();
    formData.append('photo', files[0]);

    this.customerService
      .addUserPhoto(formData, this.customerId)
      .subscribe((res: any) => {
        this.messageService.add({
          severity: 'success',
          summary: res.message,
          closable: true
        });
      });

    return (this.files = files);
  }

  public deletePhoto() {
    if (this.files || this.customer.photo_path) {
      this.customerService
        .deleteUserPhoto(this.customerId)
        .subscribe((res: any) => {
          this.files = null;
          this.customer.photo_path = null;

          this.messageService.add({
            severity: 'success',
            summary: res.message,
            closable: true
          });
        });
    }
  }

  public suspendCustomer() {
    this.addEditCustomerForm.get('status').patchValue('2');
    this.addEditCustomer();
  }

  public pauseClient() {
    this.customerService
      .pauseCampaigns(this.customerId)
      .subscribe((res: any) => {
        this.messageService.add({
          severity: 'success',
          summary: res.message,
          closable: true
        });
      });
  }

  private getCustomer() {
    if (this.customerId) {
      this.customerService
        .getCustomer(this.customerId)
        .subscribe((res: any) => {
          res.data.status = res.data.status.toString();
          this.customer = res.data;
          this.addEditCustomerForm.patchValue(res.data);
          this.loading = false;
        });
    } else {
      this.loading = false;
    }
  }

  private getPlans() {
    this.customerService.getPlans().subscribe((res: any) => {
      this.plansList = res.data.map(plan => {
        return {
          label: `${plan.title} ($${plan.price}/month)`,
          value: plan.name
        };
      })
    });
  }
}
