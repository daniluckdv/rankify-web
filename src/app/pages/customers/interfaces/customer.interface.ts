interface ICreateUpdateCustomer {
  readonly first_name: string;
  readonly last_name: string;
  readonly business_name: string;
  readonly username: string;
  readonly email: string;
  readonly phone: string;
  readonly status: string;
  readonly comment?: string;
  readonly password?: string;
  readonly level?: string;
  _method?: string;
}
