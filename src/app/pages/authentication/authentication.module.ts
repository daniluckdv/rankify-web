import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { InputTextModule } from 'primeng/inputtext';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { AuthenticationRoutingModule } from './authentication-routing.module';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { AuthenticationService } from './services/authentication.service';
import { AuthLayoutComponent } from '../../layouts/auth-layout/auth-layout.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    AuthLayoutComponent,
    SignInComponent,
    SignUpComponent,
    ForgotPasswordComponent
  ],
  imports: [
    InputTextModule,
    CommonModule,
    ReactiveFormsModule,
    ButtonModule,
    AuthenticationRoutingModule,
    ToastModule,
    SharedModule
  ],
  providers: [MessageService, AuthenticationService]
})
export class AuthenticationModule {}
