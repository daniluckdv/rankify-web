interface ILoginBody {
  readonly email: string;
  readonly password: string;
};

interface ISignUpBody {
  readonly first_name: string;
  readonly last_name: string;
  readonly business_name: string;
  readonly username: string;
  readonly password: string;
  readonly email: string;
  readonly phone: string;
}
