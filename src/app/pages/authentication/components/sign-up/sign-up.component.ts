import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '../../services/authentication.service';
import { UserService } from 'src/app/core/services/user.service';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  public signUpForm: FormGroup;
  public disable = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthenticationService,
    private userService: UserService,
    private messageService: MessageService,
    private router: Router
  ) {
    this.signUpForm = this.formBuilder.group({
      first_name: [null, Validators.compose([Validators.required])],
      last_name: [null, Validators.compose([Validators.required])],
      business_name: [null, Validators.compose([Validators.required])],
      username: [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required])],
      email: [
        null,
        Validators.compose([Validators.required, Validators.email])
      ],
      phone: [null, Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {}

  public signUp() {
    this.disable = true;
    this.authService.signUp(this.signUpForm.value).subscribe(
      (res: any) => {
        this.disable = false;
        localStorage.setItem('token', res.data.access_token);
        this.setUser(res.data.user.id);
      },
      (thrown: any) => {
        this.disable = false;
        this.messageService.add({
          severity: 'error',
          summary: thrown.error.error
        });
      }
    );
  }

  private setUser(userId: string) {
    this.userService.getUserSettings(userId).subscribe((res: any) => {
      res.data.role = res.data.level;
      localStorage.setItem('user', JSON.stringify(res.data));
      this.router.navigate(['/dashboard']);
    });
  }
}
