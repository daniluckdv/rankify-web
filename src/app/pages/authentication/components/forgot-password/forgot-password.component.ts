import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '../../services/authentication.service';
import { MessageService } from 'primeng/api';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  public rememberPasswordForm: FormGroup;
  public disable = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthenticationService,
    private messageService: MessageService,
    private router: Router
  ) {
    this.rememberPasswordForm = this.formBuilder.group({
      email: [null, Validators.compose([Validators.required, Validators.email])]
    });
  }

  ngOnInit() {}

  public rememberPassword() {
    this.disable = true;

    this.authService.resetPassword(this.rememberPasswordForm.value).subscribe(
      (res: any) => {
        this.disable = false;
        this.router.navigate(['/sign-in']);

        this.messageService.add({
          severity: 'success',
          summary: res.message
        });
      },
      () => (this.disable = false)
    );
  }
}
