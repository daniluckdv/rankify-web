import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { AuthenticationService } from '../../services/authentication.service';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  public signInForm: FormGroup;
  public disable: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private messageService: MessageService,
    private authService: AuthenticationService,
    private userService: UserService
  ) {
    this.signInForm = this.formBuilder.group({
      email: [
        null,
        Validators.compose([Validators.minLength(3), Validators.required])
      ],
      password: [
        null,
        Validators.compose([Validators.minLength(6), Validators.required])
      ]
    });
  }

  ngOnInit() {}

  public signIn() {
    const formData = this.signInForm.value;
    this.authService
      .signIn({ email: formData.email, password: formData.password })
      .subscribe(
        (res: any) => {
          this.disable = true;
          localStorage.setItem('token', res.data.access_token);
          this.setUser(res.data.user.id);
        },
        (thrown: any) => {
          this.messageService.add({
            severity: 'error',
            summary: thrown.error.error
          });
        }
      );
  }

  private setUser(userId: string) {
    this.userService.getUserSettings(userId).subscribe((res: any) => {
      res.data.role = res.data.level;
      localStorage.setItem('user', JSON.stringify(res.data));
      this.router.navigate(['/dashboard']);
    });
  }
}
