import { Injectable } from '@angular/core';
import { ApiService } from '../../../core/services/api.service';

@Injectable()
export class AuthenticationService {
  constructor(private api: ApiService) {}

  public signIn(body: ILoginBody) {
    return this.api.request('POST', 'login', body, false);
  }
  
  public signUp(body: ISignUpBody) {
    return this.api.request('POST', 'signup', body, false);
  }
  
  public resetPassword(body: object) {
    return this.api.request('POST', 'reset-password', body, false);
  }
}
